package main

import "golang.org/x/tour/pic"

func Pic(dx, dy int) [][]uint8 {
	s := make([][]uint8, dy)
	for i := range s {
		s[i] = make([]uint8, dx)
	}
	for i := range s {
		for j := range s[i] {
			/* This Checkerboard pattern was too small, so I need larger block ranges
			if (i + j) % 2 == 0 {
				s[i][j] = 120
			} else {
				s[i][j] = 0
			}
			*/
			s[i][j] = uint8(i) * uint8(j)
		}
	}
	return s
}

func main() {
	pic.Show(Pic)
}
